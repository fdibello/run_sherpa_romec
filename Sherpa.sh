#!/bin/bash
date
hostname

echo 'job number ',$JOB_NUMBER
echo 'data dir',$DATA_DIR
echo 'files to copy',$FILES2COPY
echo 'RND',$RNDM

tmpDir=`mktemp -d`
cd ${tmpDir}
echo 'we are in '${PWD}

scp -r  /atlashome/dibellof/Generate  .
cd Generate

#pwd
#cd /atlashome/dibellof/Generate
#pwd
#source setup.sh
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
asetup AthGeneration,21.6.18, here


#4FS used to produe fuse sample
Gen_tf.py --ecmEnergy=13000.0 --randomSeed=${RNDM} --jobConfig=950024 --outputEVNTFile=tmp.EVNT.root --maxEvents=1000
#NOMINAL 5FS
#Gen_tf.py --ecmEnergy=13000.0 --randomSeed=${RNDM} --jobConfig=421304 --outputEVNTFile=tmp.EVNT.root --maxEvents=1000

scp tmp.EVNT.root  $FILES2COPY/fdibello_EVNT_950024_${JOB_NUMBER}_EVNT.root
#scp tmp.EVNT.root  /storage/ceph/data/atlas/user/dibellof/EVNT/fdibello_EVNT_950024_${JOB_NUMBER}_EVNT.root


date
echo 'done, now clean up'

cd
rm -rf ${tmpDir}

 
