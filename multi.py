import os, sys, commands, re, random
from stat import *

dataSetDir='/storage/ceph/data/atlas/user/dibellof/EVNT'
logFile = '/storage/ceph/data/atlas/user/dibellof/log/'

if '/'!=dataSetDir[-1]: dataSetDir+='/'
jscript=os.environ['PWD']+'/Sherpa.sh'
os.environ['DATA_DIR']=dataSetDir
random=1235
que='atlaslong'

Njobs = 1
jn = 0
for fn in range(0,Njobs):

        jn+=1
        random+=1
        sjn='%04d' % jn
        os.environ['JOB_NUMBER']=sjn
        os.environ['FILES2COPY']=dataSetDir
        os.environ['RNDM']=str(random)
        print 'submitting job:',os.environ['JOB_NUMBER']
        cmd = "bsub -q "+que+" -o "+" "+logFile+" -e "+" "+logFile+" sh "+jscript
        #cmd = "qsub -q "+que+" -N MV2_Training"+os.environ['JOB_NUMBER']+" -l cput=72:00:00 -l pmem=10000mb -l pvmem=10000mb -l mem=10000mb -l vmem=10000mb"+" -v 'JOB_NUMBER,DATA_DIR,FILES2COPY' "+jscript

        print "now do",cmd
        ou = commands.getoutput(cmd)
        "job",os.environ['JOB_NUMBER'],"submitted as",ou


print 'submission done'
